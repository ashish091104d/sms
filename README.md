**Student Management System**
-
Student management system(SMS) is a spring boot based rest API, 
which provide the support to perform the CRUD operations on Student object

**API supported**
-
1. getAllStudent: 
    >       /api/v1/students

1. getStudent: 
    >       /api/v1/students/{id}
1. saveStudent: 
    >       /api/v1/students
                    {
                      "classIn": "Four B",
                      "firstName": "Peter",
                      "id": 1,
                      "lastName": "John",
                      "nationality": "USA"
                    }
1. updateStudent: 
    >       /api/v1/students  
                      {
                        "classIn": "Four B",
                        "firstName": "Peter",
                        "id": 1,
                        "lastName": "John",
                        "nationality": "USA"
                      }                                       
1. deleteStudent: 
    >       /api/v1/students/{id}
 

**DB Details**
-
http://localhost:8080/h2-ui/

Jdbc Url : <jdbc:h2:mem:testdb>

Username : sa

password: <leave empty> 

**Swagger Details**
- 
http://localhost:8080/swagger-ui.html#/


**Start the application**
-
To start the SMS(Student management system) please run SmsApplication.java 