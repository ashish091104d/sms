package com.sc.assessment.sms.exception;

public class SMSException extends RuntimeException{
    private String code;
    private String message;

    public SMSException() {
        super();
    }

    public SMSException(String message) {
        super(message);
        this.code = "";
        this.message = message;
    }

    public SMSException(String code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

}
