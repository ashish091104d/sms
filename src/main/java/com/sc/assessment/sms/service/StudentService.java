package com.sc.assessment.sms.service;

import com.sc.assessment.sms.entity.Student;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

public interface StudentService {

    List<Student> getAllStudents();

    Optional<Student> getStudent(Long studentId);

    Student saveStudent(Student student);

    Student updateStudent(Student student) throws Exception;

    void deleteStudent(Student student);

}
