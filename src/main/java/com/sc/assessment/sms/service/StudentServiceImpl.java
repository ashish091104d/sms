package com.sc.assessment.sms.service;

import com.sc.assessment.sms.entity.Student;
import com.sc.assessment.sms.exception.SMSException;
import com.sc.assessment.sms.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService{

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public List<Student> getAllStudents() {
        List<Student> studentList = studentRepository.findAll();
        return studentList;
    }

    @Override
    public Optional<Student> getStudent(Long studentId) {
        Optional<Student> optionalStudent = studentRepository.findById(studentId);
        return optionalStudent;
    }

    @Override
    public Student saveStudent(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public Student updateStudent(Student student) throws Exception {
        Optional<Student> studentInDb = studentRepository.findById(student.getId());
        if(!studentInDb.isPresent()) {
            throw new SMSException("Could not update as provided student is not in our record");
        }
        return studentRepository.save(student);
    }

    @Override
    public void deleteStudent(Student student) {
        studentRepository.delete(student);
    }
}
