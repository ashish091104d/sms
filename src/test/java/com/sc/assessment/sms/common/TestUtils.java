package com.sc.assessment.sms.common;

import com.sc.assessment.sms.entity.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TestUtils {

    public static final Long ID = 1L;
    public static final String FIRST_NAME = "Ashish";
    public static final String LAST_NAME = "Kumar";
    public static final String NATIONALITY = "Indian";
    public static final String CLASS_IN = "B Tech";

    public static List<Student> getAllMockStudents() {
        List<Student> studentList = new ArrayList<>();
        Student student = new Student();
        student.setId(ID);
        student.setFirstName(FIRST_NAME);
        student.setLastName(LAST_NAME);
        student.setNationality(NATIONALITY);
        student.setClassIn(CLASS_IN);
        studentList.add(student);

        return studentList;
    }

    public static Student getMockStudent(Long id) {
        Student student = new Student();
        student.setId(id);
        student.setFirstName(FIRST_NAME);
        student.setLastName(LAST_NAME);
        student.setNationality(NATIONALITY);
        student.setClassIn(CLASS_IN);

        return student;
    }

    public static Student getMockStudent() {
        return getMockStudent(1L);
    }
}
