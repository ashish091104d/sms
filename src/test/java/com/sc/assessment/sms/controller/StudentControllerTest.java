package com.sc.assessment.sms.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.sc.assessment.sms.common.TestUtils;
import com.sc.assessment.sms.entity.Student;
import com.sc.assessment.sms.service.StudentService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class StudentControllerTest {

    private ObjectWriter objectMapper;

    @Mock
    private StudentService studentService;
    private MockMvc mockMvc;

    @Before
    public void setup(){
        objectMapper = new ObjectMapper().writerFor(Student.class) ;
        StudentController studentController =  new StudentController(studentService);
        mockMvc = MockMvcBuilders.standaloneSetup(studentController).build();

    }

    @Test
    public void getAllStudents() throws Exception {
        given(studentService.getAllStudents()).willReturn(TestUtils.getAllMockStudents());
        MvcResult result = this.mockMvc.perform(get("/api/v1/students")).andExpect(status().isOk()).andReturn();

    }

    @Test
    public void getStudent() throws Exception {
        given(studentService.getStudent(1L)).willReturn(Optional.ofNullable(TestUtils.getMockStudent(1L)));
        MvcResult result = this.mockMvc.perform(get("/api/v1/students/1")).andExpect(status().isOk()).andReturn();

    }

    @Test
    public void saveStudent() throws Exception {
        String studentInputContent = objectMapper.writeValueAsString(TestUtils.getMockStudent());
        MvcResult result = this.mockMvc.perform(post("/api/v1/students")
                .content(studentInputContent)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated()).andReturn();

    }

    @Test
    public void updateStudent() throws Exception {
        String studentInputContent = objectMapper.writeValueAsString(TestUtils.getMockStudent());
        MvcResult result = this.mockMvc.perform(put("/api/v1/students")
                .content(studentInputContent)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

    }

    @Test
    public void deleteStudent() throws Exception {
        String studentInputContent = objectMapper.writeValueAsString(TestUtils.getMockStudent());
        MvcResult result = this.mockMvc.perform(delete("/api/v1/students/2")).andExpect(status().isNoContent()).andReturn();

    }

}
