package com.sc.assessment.sms.service;

import com.sc.assessment.sms.common.TestUtils;
import com.sc.assessment.sms.entity.Student;
import com.sc.assessment.sms.repository.StudentRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.Mockito.*;
import static com.sc.assessment.sms.common.TestUtils.*;

import java.util.List;
import java.util.Optional;

@SpringBootTest
public class StudentServiceImplTest {

    @InjectMocks
    StudentServiceImpl studentService;

    @Mock
    StudentRepository studentRepository;


    @Test
    public void getAllStudents(){
        when(studentRepository.findAll()).thenReturn(TestUtils.getAllMockStudents());

        List<Student> students = studentService.getAllStudents();
        Assertions.assertThat(students).isNotNull();
        Assertions.assertThat(students.size()).isEqualTo(1);
        Assertions.assertThat(students.get(0)).isNotNull();
        Assertions.assertThat(students.get(0).getId()).isEqualTo(ID);
        Assertions.assertThat(students.get(0).getFirstName()).isEqualTo(FIRST_NAME);
        Assertions.assertThat(students.get(0).getLastName()).isEqualTo(LAST_NAME);
        Assertions.assertThat(students.get(0).getNationality()).isEqualTo(NATIONALITY);
        Assertions.assertThat(students.get(0).getClassIn()).isEqualTo(CLASS_IN);
    }

    @Test
    public void getStudent(){
        when(studentRepository.findById(anyLong())).thenReturn(Optional.ofNullable(TestUtils.getMockStudent()));

        Optional<Student> optionalStudent = studentService.getStudent(1L);
        Assertions.assertThat(optionalStudent).isPresent();
        Assertions.assertThat(optionalStudent.get()).isNotNull();
        Assertions.assertThat(optionalStudent.get().getId()).isEqualTo(ID);
        Assertions.assertThat(optionalStudent.get().getFirstName()).isEqualTo(FIRST_NAME);
        Assertions.assertThat(optionalStudent.get().getLastName()).isEqualTo(LAST_NAME);
        Assertions.assertThat(optionalStudent.get().getNationality()).isEqualTo(NATIONALITY);
        Assertions.assertThat(optionalStudent.get().getClassIn()).isEqualTo(CLASS_IN);
    }

    @Test
    public void saveStudent(){
        when(studentRepository.save(any())).thenReturn(TestUtils.getMockStudent());

        Student optionalStudent = studentService.saveStudent(TestUtils.getMockStudent());

        Assertions.assertThat(optionalStudent).isNotNull();
        Assertions.assertThat(optionalStudent.getId()).isEqualTo(ID);
        Assertions.assertThat(optionalStudent.getFirstName()).isEqualTo(FIRST_NAME);
        Assertions.assertThat(optionalStudent.getLastName()).isEqualTo(LAST_NAME);
        Assertions.assertThat(optionalStudent.getNationality()).isEqualTo(NATIONALITY);
        Assertions.assertThat(optionalStudent.getClassIn()).isEqualTo(CLASS_IN);
    }

    @Test
    public void updateStudent() throws Exception {
        when(studentRepository.findById(anyLong())).thenReturn(Optional.ofNullable(TestUtils.getMockStudent()));
        when(studentRepository.save(any())).thenReturn(TestUtils.getMockStudent());

        Student optionalStudent = studentService.updateStudent(TestUtils.getMockStudent());

        Assertions.assertThat(optionalStudent).isNotNull();
        Assertions.assertThat(optionalStudent.getId()).isEqualTo(ID);
        Assertions.assertThat(optionalStudent.getFirstName()).isEqualTo(FIRST_NAME);
        Assertions.assertThat(optionalStudent.getLastName()).isEqualTo(LAST_NAME);
        Assertions.assertThat(optionalStudent.getNationality()).isEqualTo(NATIONALITY);
        Assertions.assertThat(optionalStudent.getClassIn()).isEqualTo(CLASS_IN);
    }

    @Test
    public void deleteStudent() throws Exception {
        studentService.deleteStudent(TestUtils.getMockStudent());
    }

}
